if (([System.Environment]::OSVersion.Platform) -eq "Win32NT")
# Running in Windows
{
	# Copying neovim config
	$nvimDst = (Join-Path -Path $env:LOCALAPPDATA -ChildPath "nvim\init.vim")
	$null = New-Item -Force -Path (Split-Path -Path $nvimDst) -ItemType "directory"
	Copy-Item (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\nvim\init.vim") -Destination $nvimDst
	Invoke-WebRequest -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim | New-Item "$(@($env:XDG_DATA_HOME, $env:LOCALAPPDATA)[$null -eq $env:XDG_DATA_HOME])/nvim-data/site/autoload/plug.vim" -Force
	if (Get-Command nvim -ErrorAction SilentlyContinue)
	{
		nvim --headless +PlugInstall +qall
	}

	# Wezterm config file
	Copy-Item (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\wezterm\wezterm.lua") -Destination "$HOME\.wezterm.lua"

	# Visual Studio Code config file
	$codeDst = (Join-Path -Path $env:APPDATA -ChildPath "Code\User\settings.json")
	$null = New-Item -Force -Path (Split-Path -Path $codeDst) -ItemType "directory"
	Copy-Item (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\Code\User\settings.json") -Destination $codeDst

	# CherryTree config file
	$cherrytreeDst = (Join-Path -Path $env:LOCALAPPDATA -ChildPath "cherrytree\config.cfg")
	$null = New-Item -Force -Path (Split-Path -Path $cherrytreeDst) -ItemType "directory"
	Copy-Item (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\cherrytree\config.cfg") -Destination $cherrytreeDst

	# Powershell theme using oh-my-posh
	$ProfileSrc = (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\powershell\profile.ps1")
	$ThemeSrc = (Join-Path -Path $PSScriptRoot -ChildPath "private_dot_config\powershell\theme.omp.json")

	$id = [System.Security.Principal.WindowsIdentity]::GetCurrent()
	$p = New-Object System.Security.Principal.WindowsPrincipal($id)

	if ($p.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator))
	{
		if ( ! (Get-Module -ListAvailable -Name Terminal-Icons))
		{
			Install-Module -Name Terminal-Icons -Force
		}
		if (!(Get-Command "oh-my-posh" -ErrorAction SilentlyContinue))
		{
			if (Get-Command "winget" -ErrorAction SilentlyContinue)
			{
				winget install -e --id JanDeDobbeleer.OhMyPosh
			}
			elseif (Get-Command "choco" -ErrorAction SilentlyContinue)
			{
				choco install -y oh-my-posh
			}
			else
			{
				Write-Host "Oh-my-posh is not installed and winget or choco are not available. Exiting". -ForegroundColor "Red"
				Return
			}
		}
		Write-Host "Installing PowerShell profile for all users." -ForegroundColor "Cyan"
		if ($PSVersionTable.PSEdition -eq "Desktop")
		{
			if (Get-Command "pwsh.exe" -ErrorAction SilentlyContinue)
			{
				$pwshProfile = pwsh -NoProfile -Command {$PROFILE.AllUsersAllHosts}
				Try
				{
					[io.file]::OpenWrite($pwshProfile).close()
				}
				Catch
				{
					Write-Host "Unable to write to $pwshProfile". -Foreground "Yellow"
					$pwshProfile = pwsh -NoProfile -Command {$PROFILE.CurrentUserAllHosts}
					Write-Host "Writing to $pwshProfile". -Foreground "Yellow"
				}
			}
			else
			{
				$pwshProfile = "$env:ProgramFiles\PowerShell\7\profile.ps1"
				Write-Host "pwsh is not available. Using $pwshProfile as a default location for the profile." -Foreground "Yellow"
			}
			$powershellProfile = $PROFILE.AllUsersAllHosts
		}
		elseif ($PSVersionTable.PSEdition -eq "Core")
		{
			$pwshProfile = $PROFILE.AllUsersAllHosts
			Try
			{
				[io.file]::OpenWrite($pwshProfile).close()
			}
			Catch
			{
				Write-Host "Unable to write to $pwshProfile". -Foreground "Yellow"
				$pwshProfile = $PROFILE.CurrentUserAllHosts
				Write-Host "Writing to $pwshProfile". -Foreground "Yellow"
			}
			$powershellProfile = powershell -NoProfile -Command {$PROFILE.AllUsersAllHosts}
		}
	}
	else
	{
		Write-Host "The script is not run with Administrator rights." -ForegroundColor "Yellow"
		if ((Get-Module -ListAvailable -Name Terminal-Icons) -and (Get-Command "oh-my-posh" -ErrorAction SilentlyContinue))
		{
			Write-Host "Installing for the current user $env:UserName only." -ForegroundColor "Cyan"
			if ($PSVersionTable.PSEdition -eq "Desktop")
			{
				if (Get-Command "pwsh.exe" -ErrorAction SilentlyContinue)
				{
					$pwshProfile = pwsh -NoProfile -Command {$PROFILE.CurrentUserAllHosts }
				}
				else
				{
					$pwshProfile = (Join-Path -Path $env:USERPROFILE -ChildPath 'Documents\PowerShell\profile.ps1')
				}
				$powershellProfile = $PROFILE.CurrentUserAllHosts
			}
			elseif ($PSVersionTable.PSEdition -eq "Core")
			{
				$pwshProfile = $PROFILE.CurrentUserAllHosts
				$powershellProfile = powershell -NoProfile -Command {$PROFILE.CurrentUserAllHosts}
			}
		}
		else
		{
			Write-Host "The Terminal-Icons module and/or oh-my-posh are not installed. Exiting." -ForegroundColor "Red"
			Return
		}

	}
	# Copying the files into the PowerShell Desktop directories.
	$powershellDst = (Split-Path -Path $powershellProfile)
	$powershellTheme = (Join-Path -Path $powershellDst -ChildPath "theme.omp.json")
	$null = New-Item -Force -Path $powershellDst -ItemType "directory"
	if ((Test-Path "$powershellProfile" -PathType leaf ))
	{
		Write-Host "Replacing the file at $powershellProfile." -ForegroundColor "Yellow"
		Remove-Item -Recurse -Force $powershellProfile
	}
	Copy-Item $ProfileSrc -Destination $powershellProfile
	if ((Test-Path "$powershellTheme" -PathType leaf ))
	{
		Write-Host "Replacing the file at $powershellTheme." -ForegroundColor "Yellow"
		Remove-Item -Recurse -Force $powershellTheme
	}
	Copy-Item $ThemeSrc -Destination $powershellTheme


	# Creating symbolic links for PowerShell Core
	$pwshDst = (Split-Path -Path $pwshProfile)
	$pwshTheme = (Join-Path -Path $pwshDst -ChildPath "theme.omp.json")
	$null = New-Item -Force -Path $pwshDst -ItemType "directory"
	if ($p.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator))
	{
		New-Item -Force -ItemType SymbolicLink -Path $pwshProfile -Target $powershellProfile
		New-Item -Force -ItemType SymbolicLink -Path $pwshTheme -Target $powershellTheme
	}
	else
	{
		Copy-Item $ProfileSrc -Destination $pwshProfile
		Copy-Item $ThemeSrc -Destination $pwshTheme
	}
}
elseif (([System.Environment]::OSVersion.Platform) -eq "Unix")
{
	Write-Host 'Running in Unix. Install using the "install.sh" script.' -ForegroundColor "Red"
}