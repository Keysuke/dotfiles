#!/bin/sh

echo "Removing existing nvim and zinit config files."

rm -rf ~/.config/nvim ~/.local/share/zinit ~/.zi