#!/bin/sh
echo "Installing home dotfiles for user $USER"

INSTALL_DIR=$(cd "$(dirname "$0")" && pwd)

sh $INSTALL_DIR/run_once_before_install-cache.sh

mkdir -p ~/.config
for file in $INSTALL_DIR/*dot* ; do
	if [ -d "$file" ] ; then
		cp -rf "$file/." ~/$(echo $file | awk -F "private_" '{print $NF}' | awk -F "dot_" '{print "." $NF}')
	else
		cp -rf "$file" ~/$(echo $file | awk -F "private_" '{print $NF}' | awk -F "dot_" '{print "." $NF}')
	fi
done

cd ~/.config
rm -f "symlink_Code - OSS"
ln -sf Code "Code - OSS"

cd ~/.config/powershell
rm -f symlink_Microsoft.PowerShell_profile.ps1
ln -sf Microsoft.PowerShell_profile.ps1 profile.ps1

sh $INSTALL_DIR/run_once_after_install-dependencies.sh

rm -rf ~/.git ~/.gitmodules ~/.gitignore