local wezterm = require 'wezterm'

return {
	prefer_egl = true,
	color_scheme = "Catppuccin Mocha",
	default_cursor_style = 'BlinkingBlock',
	check_for_updates = false,
	enable_scroll_bar = true,
	hide_tab_bar_if_only_one_tab = true,
	debug_key_events = true,
	audible_bell = Disabled,
	warn_about_missing_glyphs = false,
	keys = {
		{
			key = ')',
			mods = 'CTRL',
			action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' }
		},
		{
			key = '(',
			mods = 'CTRL',
			action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' }
		},
	}
}
