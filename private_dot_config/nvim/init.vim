set nocompatible

if empty(glob("/nix/store"))
	if has('nvim')
		let s:editor_root=expand("~/.config/nvim")
		set guicursor=
	elseif has('win32')
		let s:editor_root=expand("~/vimfiles")
	else
		let s:editor_root=expand("~/.vim")
	endif

	if has('nvim') && empty(glob('/usr/share/nvim/runtime/autoload/plug.vim')) && empty(glob(s:editor_root.'/autoload/plug.vim'))
		silent execute "!curl -fLo ".s:editor_root."/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
		autocmd VimEnter * silent PlugInstall --sync
	endif

	if !empty(glob('/usr/share/nvim/runtime/autoload/plug.vim')) || !empty(glob(s:editor_root.'/autoload/plug.vim'))
		let &rtp = &rtp . ',' . s:editor_root . '/plugged'

		call plug#begin(s:editor_root.'/plugged')

		if has('nvim-0.7')
			if empty(glob("/usr/share/nvim/runtime/colors/catppuccin.vim"))
				silent Plug 'catppuccin/nvim', { 'as': 'catppuccin' }
			endif
		else
			silent Plug 'sickill/vim-monokai'
		endif

		if empty(glob("/usr/share/vim/vimfiles/autoload/airline.vim"))
			silent Plug 'vim-airline/vim-airline'
		endif
		if empty(glob("/usr/share/vim/vimfiles/plugin/airline-themes.vim"))
			silent Plug 'vim-airline/vim-airline-themes'
		endif
		if empty(glob("/usr/share/vim/vimfiles/autoload/ctrlp.vim"))
			silent Plug 'ctrlpvim/ctrlp.vim'
		endif
		if empty(glob("/usr/share/vim/vimfiles/autoload/syntastic/util.vim"))
			silent Plug 'vim-syntastic/syntastic'
		endif
		if empty(glob("/usr/share/vim/vimfiles/plugin/surround.vim"))
			silent Plug 'tpope/vim-surround'
		endif
		if empty(glob("/usr/share/vim/vimfiles/autoload/rainbow.vim"))
			silent Plug 'luochen1990/rainbow'
		endif
		if empty(glob("/usr/share/vim/vimfiles/autoload/nerdtree.vim"))
			silent Plug 'preservim/nerdtree'
		endif
		if empty(glob("/usr/share/vim/vimfiles/autoload/nerdcommenter.vim"))
			silent Plug 'preservim/nerdcommenter'
		endif
		if empty(glob("/usr/share/vim/vimfiles/plugin/oscyank.vim"))
			silent Plug 'ojroques/vim-oscyank', {'branch': 'main'}
		endif

		call plug#end()
	endif
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=700

" Enable filetype plugins
filetype plugin on
filetype indent on

" Enable a drop-down list for completion
set omnifunc=syntaxcomplete#Complete

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Better Unix / Windows compatibility
set viewoptions=folds,options,cursor,unix,slash

" Visual mode when using Shift+arrow
set keymodel=startsel
" }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let myterm = $TERM

if !has('nvim')
	set ttymouse=xterm2
else
	let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
	let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_signs=1
let g:neocomplete#enable_at_startup = 1
let g:deoplete#enable_at_startup = 1
let g:rainbow_active = 1

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Turn on the WiLd menu
set wildmenu

" Command <Tab> completion, list matches, then longest common part, then all
set wildmode=list:longest,full

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

" Height of the command bar
set cmdheight=2


if has('cmdline_info')
	set ruler                   " Show the ruler
	set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
	set showcmd                 " Show partial commands in status line and
								" Selected characters/lines in visual mode
endif

" A buffer becomes hidden when it is abandoned
set hidden

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

set belloff=all
set t_vb=
set tm=500
set number
set hlsearch
set incsearch
set mouse=a
set pastetoggle=<F2>
set foldenable

" Highlight current line
set cursorline

" Hide the mouse cursor while typing
set mousehide

" Rebind clipboard using vim-oscyank
let g:oscyank_max_length = 0  " maximum length of a selection
let g:oscyank_silent     = 1  " disable message on successful copy
let g:oscyank_trim       = 1  " trim surrounding whitespaces before copy
autocmd TextYankPost * if $SSH_CLIENT != '' && exists(":OSCYank") && v:event.operator is 'y' && v:event.regname is '' | execute 'OSCYankRegister "' | endif

" Use the clipboard
if (myterm != "linux" && has('clipboard'))
	if has('unnamedplus') || has("nvim") " When possible use + register for copy-paste
		set clipboard^=unnamedplus,unnamed
	else	" On mac and Windows, use * register for copy-paste
		set clipboard^=unnamed
	endif
endif

" }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors and Fonts {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set extra options when running in GUI mode
if has("gui_running")
	set guioptions+=e
	set guitablabel=%M\ %t

	if (has("gui_gtk3") || has("gui_gtk2"))
		set guifont=DejaVu\ Sans\ Mono\ 9.5
	elseif has("gui_win32")
		set guifont=Consolas:h11:cANSI
	endif
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Enable syntax highlighting
syntax enable
" }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile
au BufNewFile,BufRead *.log set filetype=messages
au BufNewFile,BufRead /var/log/* set filetype=messages
" }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
" set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines


" }

""""""""""""""""""""""""""""""
" => Visual mode related {
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>
" }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Close the current buffer
map <leader>bd :Bclose<cr>

" Close all the buffers
map <leader>ba :1,1000 bd!<cr>

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Specify the behavior when switching between buffers
try
	set switchbuf=useopen,usetab,newtab
	set stal=2
catch
endtry

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\ exe "normal! g`\"" |
	\ endif
" Remember info about open buffers on close
set viminfo^=%

" }


""""""""""""""""""""""""""""""
" => Status line {
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

highlight Normal ctermbg=none
highlight NonText ctermbg=none

if ( myterm =~ "linux" || myterm =~ "vt100" || myterm =~ "vt102" || myterm =~ "vt220" )
	colorscheme desert
	hi User1	ctermbg=1		ctermfg=0
	hi User2	ctermbg=3		ctermfg=0
	hi User3	ctermbg=2		ctermfg=0
	hi User4	ctermbg=2		ctermfg=0
	hi User5	ctermbg=6		ctermfg=0
	hi User7	ctermbg=1		ctermfg=7		cterm=bold
	hi User8	ctermbg=4		ctermfg=0
	hi User9	ctermbg=5		ctermfg=0
	hi User0	ctermbg=7		ctermfg=0
endif

if ( myterm =~ "xterm" || myterm =~ "rxvt" || has("gui_running"))
	set t_Co=256
	set termguicolors

	if has('nvim-0.7')
		silent! colorscheme catppuccin
		let g:airline_theme='catppuccin'
	else
		silent! colorscheme monokai
		let g:airline_theme='molokai'
	endif

	hi User1	ctermbg=52		ctermfg=15
	hi User2	ctermbg=130		ctermfg=15
	hi User3	ctermbg=94		ctermfg=15
	hi User4	ctermbg=22		ctermfg=15
	hi User5	ctermbg=36		ctermfg=15
	hi User7	ctermbg=89		ctermfg=15
	hi User8	ctermbg=17		ctermfg=15
	hi User9	ctermbg=53		ctermfg=15
	hi User0	ctermbg=15		ctermfg=15

	hi User1	guibg=#880c0e	guifg=#000000
	hi User2	guibg=#d75f00	guifg=#000000
	hi User3	guibg=#ffaf00	guifg=#000000
	hi User4	guibg=#aefe7b	guifg=#000000
	hi User5	guibg=#5f8787	guifg=#000000
	hi User7	guibg=#880c0e	guifg=#ffffff
	hi User8	guibg=#0087d7	guifg=#000000
	hi User9	guibg=#5f0087	guifg=#000000
	hi User0	guibg=#094afe	guifg=#000000
else
	colorscheme desert
endif


function! HighlightSearch()
	if &hls
		return 'H'
	  else
		return ''
	endif
endfunction


" Airline
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 0

set listchars=tab:▸\ ,trail:•,extends:»,precedes:«,nbsp:☠,eol:¬ " Unprintable chars mapping
set list          " Display unprintable characters f12 - switches
" }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

" Move a line of text using ALT+[jk] or Comamnd+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
	nmap <D-j> <M-j>
	nmap <D-k> <M-k>
	vmap <D-j> <M-j>
	vmap <D-k> <M-k>
endif

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
	exe "normal mz"
	%s/\s\+$//ge
	exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()
" }

iabbrev </ </<C-X><C-O>
imap <C-Space> <C-X><C-O>
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" Quickly indent and unindent
nnoremap <Tab> >>_
nnoremap <S-Tab> <<_
inoremap <S-Tab> <C-D>
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

imap <C-d> <esc>ddi
imap <C-u> <esc>caw

if has('unnamedplus')
  nnoremap <leader>d "+d
  nnoremap <leader>D "+D
  vnoremap <leader>d "+d
else
  nnoremap <leader>d "*d
  nnoremap <leader>D "*D
  vnoremap <leader>d "*d
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => vimgrep searching and cope displaying {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" When you press gv you vimgrep after the selected text
vnoremap <silent> gv :call VisualSelection('gv')<CR>

" Open vimgrep and put the cursor in the right position
map <leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>

" Vimgreps in the current file
map <leader><space> :vimgrep // <C-R>%<C-A><right><right><right><right><right><right><right><right><right>

" When you press <leader>r you can search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace')<CR>

" Do :help cope if you are unsure what cope is. It's super useful!
"
" When you search with vimgrep, display your results in cope by doing:
"   <leader>cc
"
" To go to the next search result do:
"   <leader>n
"
" To go to the previous search results do:
"   <leader>p
"
map <leader>cc :botright cope<cr>
map <leader>co ggVGy:tabnew<cr>:set syntax=qf<cr>pgg
map <leader>n :cn<cr>
map <leader>p :cp<cr>

" }



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=
" }



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Quickly open a buffer for scripbble
map <leader>q :e ~/buffer<cr>

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>

" }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! CmdLine(str)
	exe "menu Foo.Bar :" . a:str
	emenu Foo.Bar
	unmenu Foo
endfunction

function! VisualSelection(direction) range
	let l:saved_reg = @"
	execute "normal! vgvy"

	let l:pattern = escape(@", '\\/.*$^~[]')
	let l:pattern = substitute(l:pattern, "\n$", "", "")

	if a:direction == 'b'
		execute "normal ?" . l:pattern . "^M"
	elseif a:direction == 'gv'
		call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
	elseif a:direction == 'replace'
		call CmdLine("%s" . '/'. l:pattern . '/')
	elseif a:direction == 'f'
		execute "normal /" . l:pattern . "^M"
	endif

	let @/ = l:pattern
	let @" = l:saved_reg
endfunction


" Returns true if paste mode is enabled
function! HasPaste()
	if &paste
		return 'PASTE MODE  '
	en
	return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
	let l:currentBufNum = bufnr("%")
	let l:alternateBufNum = bufnr("#")

	if buflisted(l:alternateBufNum)
		buffer #
	else
		bnext
	endif

	if bufnr("%") == l:currentBufNum
		new
	endif

	if buflisted(l:currentBufNum)
		execute("bdelete! ".l:currentBufNum)
	endif
endfunction

" }
