#!/bin/sh
# sh -c "$(curl -fsSL https://gitlab.com/Keysuke/dotfiles/-/raw/master/autoinstall.sh)"


if ! command -v curl >/dev/null 2>&1|| ! command -v wget >/dev/null 2>&1; then
	echo "curl or wget not found. Aborting."
	exit 1
fi

if [ $(grep "^ID=" /etc/os-release | cut -d = -f 2 | tr -d '"') = "nixos" ]; then
	HOME_INSTALL_DIR=/etc/nixos/dotfiles
	echo "Detected the NixOS distribution. Downloading dotfiles to $HOME_INSTALL_DIR."
	mkdir -p ~/.config/chezmoi
	echo "{ \"sourceDir\": \"$HOME_INSTALL_DIR\" }" > ~/.config/chezmoi/chezmoi.json
fi

if command -v chezmoi 2>&1 ; then
	echo "Using chezmoi to install dotfiles."
	chezmoi init --apply https://gitlab.com/Keysuke/dotfiles.git/
else
	HOME_INSTALL_DIR=~/.local/share/chezmoi
	if mkdir -p $HOME_INSTALL_DIR && [ -w $HOME_INSTALL_DIR ]; then
		git clone https://gitlab.com/Keysuke/dotfiles.git/ $HOME_INSTALL_DIR
		sh $HOME_INSTALL_DIR/install.sh
	else
		echo "Failed creating a temporary directory at $HOME_INSTALL_DIR. Aborting."
	fi
	rm -rf $HOME_INSTALL_DIR
fi