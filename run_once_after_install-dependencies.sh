#!/bin/sh
echo "Installing dotfiles dependencies for user $USER"

if ! command -v curl >/dev/null 2>&1|| ! command -v wget >/dev/null 2>&1; then
	echo "curl or wget not found. Aborting."
	exit 1
fi

if [ ! -r ~/.histfile ]; then
	touch ~/.histfile
fi

UNAME_OS=$(uname -o)

if [ $UNAME_OS = 'FreeBSD' ]; then
	GITSTATUSD_ARCHIVE="gitstatusd-freebsd-amd64.tar.gz"
	zsh_init()
	{
		echo "zsh -c \"zsh -is <<<''\"" >> zsh_unattended.sh
		chmod +x zsh_unattended.sh
		script "/dev/null" ./zsh_unattended.sh > /dev/null
		rm -rf zsh_unattended.sh
	}
else
	GITSTATUSD_ARCHIVE="gitstatusd-linux-$(uname -m).tar.gz"
	zsh_init()
	{
		script -qec "zsh -is </dev/null" /dev/null </dev/null >/dev/null
	}
fi

# Check if zinit is needed
if [ -r /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme ] &&\
	( [ -r /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ] || [ -r /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] || [ -r /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] ) &&\
	( [ -r /usr/share/zsh/site-functions/ ] || [ -r /usr/local/share/zsh/site-functions/ ] ) &&\
	[ -r /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh ] &&\
	[ -r /usr/share/zsh/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh ] &&\
	[ -r /usr/share/LS_COLORS/dircolors.sh ];
then
	if [ -r /usr/share/zinit/zinit.zsh ] # NixOS outdated || [ -r /run/current-system/sw/share/zinit/zinit.zsh ]
	then
		ZINIT_INSTALL=false
	else
		ZINIT_INSTALL=true
	fi
else
	ZINIT_INSTALL=true
fi

if ( $ZINIT_INSTALL && sh -c "$(curl -fsSL https://raw.githubusercontent.com/z-shell/zi-src/main/lib/sh/install.sh)" > /dev/null ) || ! $ZINIT_INSTALL
then
	zsh_init
	if [ ! -x /usr/share/zsh-theme-powerlevel10k/gitstatus/usrbin/gitstatusd ] && [ ! -x /run/current-system/sw/bin/gitstatusd ] && [ ! -x ~/.cache/gitstatus/$(echo $GITSTATUSD_ARCHIVE | cut -d . -f 1) ]; then
		mkdir -p ~/.cache/gitstatus
		GITSTATUSD_URL=$(curl -s https://api.github.com/repos/romkatv/gitstatus/releases | grep $GITSTATUSD_ARCHIVE\" | grep "browser_download_url.*" |head -1 | cut -d : -f 2,3 | tr -d \" |awk '{$1=$1};1' )
		FILENAME=$(echo $GITSTATUSD_URL | awk -F/ '{print $NF}')
		wget -q $GITSTATUSD_URL
		tar xzf $FILENAME -C ~/.cache/gitstatus
		rm -rf $FILENAME
	fi
	echo "Successfully set zsh dependencies."
else
	echo "Failed setting zsh dependencies."
fi

mkdir -p ~/.config/tmux
rm -rf ~/.config/tmux/tmux.conf ~/.config/tmux/tmux.conf.local
wget -nv https://raw.githubusercontent.com/gpakosz/.tmux/master/.tmux.conf -O ~/.config/tmux/tmux.conf
wget -nv https://raw.githubusercontent.com/gpakosz/.tmux/master/.tmux.conf.local -O ~/.config/tmux/tmux.conf.local


if [ -x "$(command -v nvim)" ]; then
	nvim --headless +PlugInstall +qall
elif [ -x "$(command -v vim)" ]; then
	vim -es -u ~/.vimrc -i NONE -c "PlugInstall" -c "qa"
fi

if [ -f /usr/share/gef/gef.py ]; then
	echo source /usr/share/gef/gef.py > ~/.gdbinit
else
	rm -rf ~/.gdbinit-gef.py
	wget -nv https://gef.blah.cat/py -O ~/.gdbinit-gef.py
	echo source ~/.gdbinit-gef.py > ~/.gdbinit
fi
echo 'set debuginfod enabled off' >> ~/.gdbinit