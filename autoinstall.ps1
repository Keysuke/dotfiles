# Invoke-Expression(New-Object Net.WebClient).downloadString('https://gitlab.com/Keysuke/dotfiles/-/raw/master/autoinstall.ps1')

$HOME_INSTALL_DIR = (Join-Path -Path $env:USERPROFILE -ChildPath "dotfiles_install")

if (New-Item -Path $HOME_INSTALL_DIR -ItemType Directory)
{
	git clone https://gitlab.com/Keysuke/dotfiles.git/ $HOME_INSTALL_DIR
	. (Join-Path -Path $HOME_INSTALL_DIR -ChildPath "install.ps1")
}
else
{
	Write-Host "Failed creating a temporary directory at $HOME_INSTALL_DIR. Aborting." -ForegroundColor "Red"
}

Remove-Item -Recurse -Force $HOME_INSTALL_DIR